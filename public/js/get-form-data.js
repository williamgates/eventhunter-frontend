function getFormData($form) {
    var indexed_array = {};
    var unindexed_array = $form.serializeArray();
    $.map(unindexed_array, function(n, i) {
        indexed_array[n["name"]] = n["value"];
    });
    return indexed_array;
}
