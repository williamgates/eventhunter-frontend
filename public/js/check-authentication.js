function checkAuthentication(authenticatedHandler, notAuthenticatedHandler, errorHandler) {
    $.ajax({
        crossDomain: true,
        type: "get",
        url: "https://eventhunter-authentication.herokuapp.com/user/check-authentication",
        beforeSend: function(xhr) {
            if (localStorage.getItem("token")) {
                xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
            }
        },
        success: function(data) {
            if (data === "true" && authenticatedHandler !== undefined) {
                authenticatedHandler();
            }
            else if (data === "false" && notAuthenticatedHandler !== undefined) {
                notAuthenticatedHandler();
            }
        },
        error: function() {
            if (errorHandler !== undefined) {
                errorHandler();
            }
        }
    });
}
