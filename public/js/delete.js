var text = generate_random_string(5)

document.getElementById("capthaBox").innerHTML= text;

var deleteButton = document.getElementById('deleteButton');

$(document).ready(function() {
    function authenticatedHandler() {
        var id = getUrlVars();
        $.ajax({
            crossDomain: true,
            type: "get",
            url: "https://eventhunter-eventmanager.herokuapp.com/purchased-tickets/details/"+id,
            success: function(data) {
                document.getElementById('eventsName').innerHTML=data.eventsName
                document.getElementById('userName').innerHTML=data.userName
                document.getElementById('ticketQuantity').innerHTML=data.quantity
            },
            error: function() {
                console.log(data)
                alert("Unknown error.");
            }
        });
    }
    function notAuthenticatedHandler() {
        alert("sign in first")
        window.location.href="/user/sign-in/"
   

    }
    function errorHandler() {
        $("body").text("Unknown authentication status.");
    }
    checkAuthentication(authenticatedHandler, notAuthenticatedHandler, errorHandler);
    
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars.id;
}

deleteButton.addEventListener('click', function() {
    var val = document.getElementById('text1').value

    if( val == text){
        deleteFunction();
    }
    else{
        alert("wrong captcha")
        text = generate_random_string(5)
        document.getElementById("capthaBox").innerHTML= text;
    }


}, false);


function deleteFunction(){
    var id = getUrlVars();

    $.ajax({
        contentType: "application/json",
        crossDomain: true,
        type: "POST",
        url: `https://eventhunter-eventmanager.herokuapp.com/purchased-tickets/delete?id=`+id,
        beforeSend: function(xhr) {
            if (localStorage.getItem("token")) {
                xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
            }
        },
        success: function(data) {
            console.log(data);
            window.location.href="/user/tickets"
            
        },
        error: function(xhr) {
            console.log(xhr);
            alert("CHECKOUT FAIL.");
        
        }
    });

}
