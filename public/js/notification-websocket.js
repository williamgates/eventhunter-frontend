var stompClient = null;

function startNotificationWebsocket() {
    $.ajax({
        crossDomain: true,
        type: "get",
        url: "https://eventhunter-eventmanager.herokuapp.com/event/get-managed-events",
        beforeSend: function(xhr) {
            if (localStorage.getItem("token")) {
                xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
            }
        },
        success: function(data) {
            var socket = new SockJS("https://eventhunter-eventmanager.herokuapp.com/eventhunter-ws");
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function (frame) {
                console.log("Connected: " + frame);
                var url = "/notification/";
                for (var i = 0; i < data.length; i++) {
                    stompClient.subscribe(url + data[i].id + "/", function (message) {
                        alert(message.body);
                    });
                }
            });
        }
    });
}

function authenticatedHandler() {
    startNotificationWebsocket();
}

$(document).ready(function() {
    checkAuthentication(authenticatedHandler); 
});
