var text = generate_random_string(5)

document.getElementById("capthaBox").innerHTML= text;

var checkout = document.getElementById('checkoutButton');

$(document).ready(function() {
    function authenticatedHandler() {
        var id = getUrlVars();
        $.ajax({
            crossDomain: true,
            type: "get",
            url: "https://eventhunter-eventmanager.herokuapp.com/event/details/"+id,
            success: function(data) {
                document.getElementById('eventsName').innerHTML=data.title
                document.getElementById('managerName').innerHTML=data.managerName
                document.getElementById('startDate').innerHTML=data.startTime
                document.getElementById('location').innerHTML=data.location
                document.getElementById('imageEvent').src=data.imageUrl
                $("#ticketQty").attr('max',data.totalTickets)
            },
            error: function() {
                alert("Unknown error.");
            }
        });
    }
    function notAuthenticatedHandler() {
        alert("sign in first")
        window.location.href="/user/sign-in/"
   

    }
    function errorHandler() {
        $("body").text("Unknown authentication status.");
    }
    checkAuthentication(authenticatedHandler, notAuthenticatedHandler, errorHandler);
    
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars.id;
}

checkout.addEventListener('click', function() {
    var val = document.getElementById('text1').value

    if( val == text){
        ajaxCall();
    }
    else{
        alert("wrong captcha")
        text = generate_random_string(5)
        document.getElementById("capthaBox").innerHTML= text;
    }


}, false);


function ajaxCall(){
    var quantity = document.getElementById('ticketQty').value
    var id = getUrlVars();
    $.ajax({
        contentType: "application/json",
        crossDomain: true,
        type: "POST",
        data:JSON.stringify(
            {
                'id': id,
                'amount':quantity,
            })
        ,
        url: `https://eventhunter-eventmanager.herokuapp.com/event/purchase-ticket`,
        beforeSend: function(xhr) {
            if (localStorage.getItem("token")) {
                xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
            }
        },
        success: function(data) {
            console.log(data);
            completeCheckout();
           
            
        },
        error: function(xhr) {

           console.log(xhr)
        
        },
    
    });

}

function completeCheckout(){
    var username = document.getElementById('userName').value
    var quantity = document.getElementById('ticketQty').value
    var eventsname  = document.getElementById('eventsName').innerHTML

    $.ajax({
        contentType: "application/json",
        crossDomain: true,
        type: "POST",
        url: `https://eventhunter-eventmanager.herokuapp.com/purchased-tickets/create?user-name=`+username+`&quantity=`+quantity+`&events-name=`+eventsname,
        beforeSend: function(xhr) {
            if (localStorage.getItem("token")) {
                xhr.setRequestHeader("Authorization", localStorage.getItem("token"));
            }
        },
        success: function(data) {
            console.log(data);
            window.location.href="/event/view-all"
            
        },
        error: function(xhr) {
            console.log(xhr);
            alert("CHECKOUT FAIL.");
        
        }
    });

}
